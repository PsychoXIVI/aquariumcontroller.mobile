import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:psychox_aquariumcontroller_mobile/util/ApplicationLocalizations.dart';

import './pages/MainPage.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        ApplicationLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        Locale('en'),
        Locale('pl'),
      ],
      onGenerateTitle: (BuildContext context) => ApplicationLocalizations.of(context).applicationTitle,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        buttonTheme: ButtonThemeData(
          buttonColor: Colors.blue[500],
          textTheme: ButtonTextTheme.primary
        ),
      ),
      home: MainPage(activeTab: MainPageTab.home),
      routes: <String, WidgetBuilder>{
        '/home': (BuildContext context) => new MainPage(activeTab: MainPageTab.home),
        '/status': (BuildContext context) => new MainPage(activeTab: MainPageTab.status),
        '/about': (BuildContext context) => new MainPage(activeTab: MainPageTab.about),
      },
    );
  }
}
