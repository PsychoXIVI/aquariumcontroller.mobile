import 'package:intl/intl.dart';

class MainPageLocalizations {
  // Menu
  String get menuHome       { return Intl.message('Main page',                name: 'MainPageLocalizations_menuHome'); }
  String get menuStatus     { return Intl.message('Overall status',           name: 'MainPageLocalizations_menuStatus'); }
  String get menuLighting   { return Intl.message('Lighting',                 name: 'MainPageLocalizations_menuLighting'); }
  String get menuHeating    { return Intl.message('Temperatures and heating', name: 'MainPageLocalizations_menuHeating'); }
  String get menuTime       { return Intl.message('Controller time',          name: 'MainPageLocalizations_menuTime'); }
  String get menuConnection { return Intl.message('Connection',               name: 'MainPageLocalizations_menuConnection'); }
  String get menuAbout      { return Intl.message('About',                    name: 'MainPageLocalizations_menuAbout'); }

  // About tab
  String get aboutCreatedBy { return Intl.message('Created by', name: 'MainPageLocalizations_aboutCreatedBy') + ' '; }
  String get aboutSeeSourceApp { return Intl.message('See Dart sources of this Flutter application', name: 'MainPageLocalizations_aboutSeeSourceApp'); }
  String get aboutSeeSourceDev { return Intl.message('See C++ sources of the Arduino controller', name: 'MainPageLocalizations_aboutSeeSourceDev'); }
  String get aboutText1 { return Intl.message('I created this application for the aquarium controller while learning the technology of mobile applications. It uses Flutter framework - Google\'s portable UI toolkit for building beautiful, natively-compiled applications for mobile, web, and desktop from a single codebase.', name: 'MainPageLocalizations_aboutText1'); }
  String get aboutText2 { return Intl.message('Whole Flutter runs by Dart language. Every element of the application, including UI, styling, translations and of course logic is written using Dart.', name: 'MainPageLocalizations_aboutText2'); }
  String get aboutText3 { return Intl.message('The application connects to Arduino-based device, which runs real aquarium controller circuits. It has serval features, such as controlling lighting, heating, reporting temperatures, time and water pH level.', name: 'MainPageLocalizations_aboutText3'); }
  String get aboutText4 { return Intl.message('The controller uses C++ to make things happen. The communication between it and the mobile application is realized over Bluetooth. There is also packet system to prevent data loss.', name: 'MainPageLocalizations_aboutText4'); }
  String get aboutText5 { return Intl.message('The controller and the application was programmed by me for my dad who has an aquarium.', name: 'MainPageLocalizations_aboutText5'); }
}
