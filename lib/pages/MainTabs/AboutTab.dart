import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:package_info/package_info.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../util/ApplicationLocalizations.dart';

class AboutTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final i10n = ApplicationLocalizations.of(context).mainPage;
    
    const imageDecoration = const BoxDecoration(
      boxShadow: [ const BoxShadow(color: Colors.black87, blurRadius: 4.0, spreadRadius: 2.0) ]
    );

    return ListView(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.fromLTRB(64.0, 16.0, 64.0, 0.0),
          child: SvgPicture.asset('images/logo.svg'),
        ),
        Container(
          margin: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('AquariumController.Mobile ', style: (TextStyle style){
                    return style.merge(TextStyle(fontWeight: FontWeight.w600));
                  }(Theme.of(context).textTheme.headline)),
                  FutureBuilder(
                    future: PackageInfo.fromPlatform(),
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      final PackageInfo packageInfo = snapshot.data;
                      final text = snapshot.hasData ? ('v' + packageInfo.version + '+' + packageInfo.buildNumber) : '...';
                      return Text(text, style: (TextStyle style){
                        return style.merge(TextStyle(color: Colors.grey));
                      }(Theme.of(context).textTheme.subhead));
                    }
                  ),
                ]
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(i10n.aboutCreatedBy),
                  Text('Patryk (PsychoX) Ludwikowski', style: const TextStyle(fontWeight: FontWeight.w600)),
                ]
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('E-mail: '),
                  GestureDetector(
                    onTap: () async {
                      const url = 'mailto:patryk.ludwikowski.7+flutter@gmail.com';
                      if (await canLaunch(url)) {
                        await launch(url);
                      }
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text('<', style: (TextStyle style){
                          return style.merge(TextStyle(color: Colors.grey));
                        }(Theme.of(context).textTheme.subhead)),
                        Text('patryk.ludwikowski.7+flutter@gmail.com'), // @TODO . mailto:
                        Text('>', style: (TextStyle style){
                          return style.merge(TextStyle(color: Colors.grey));
                        }(Theme.of(context).textTheme.subhead)),
                      ]
                    ),
                  )
                ]
              ),
            ]
          )
        ),
        Container(
          margin: const EdgeInsets.all(32.0),
          child: Divider(color: Colors.black, height: 1.0),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              child: Text(i10n.aboutSeeSourceApp),
              onPressed: () async {
                const url = 'https://gitlab.com/PsychoXIVI/aquariumcontroller.mobile';
                if (await canLaunch(url)) {
                  await launch(url);
                }
              },
            ),
            RaisedButton(
              child: Text(i10n.aboutSeeSourceDev),
              onPressed: () async {
                const url = 'https://gitlab.com/PsychoXIVI/aquariumcontroller';
                if (await canLaunch(url)) {
                  await launch(url);
                }
              },
            )
          ],
        ),
        Container(
          margin: const EdgeInsets.all(32.0),
          child: Divider(color: Colors.black, height: 1.0),
        ),
        Container(
          child: Image.asset('images/flutter_logo.png'),
          decoration: imageDecoration
        ),
        Container(
          margin: const EdgeInsets.all(16.0),
          child: RichText(
            textAlign: TextAlign.justify,
            text: TextSpan(
              text: i10n.aboutText1,
              style: DefaultTextStyle.of(context).style,
            )
          ),
        ),
        Container(
          child: Image.asset('images/dart_logo.png'),
          decoration: imageDecoration,
        ),
        Container(
          margin: const EdgeInsets.all(16.0),
          child: RichText(
            textAlign: TextAlign.justify,
            text: TextSpan(
              text: i10n.aboutText2,
              style: DefaultTextStyle.of(context).style
            )
          )
        ),
        Container(
          child: Image.asset('images/arduino_logo.png'),
          decoration: imageDecoration,
        ),
        Container(
          margin: const EdgeInsets.all(16.0),
          child: RichText(
            textAlign: TextAlign.justify,
            text: TextSpan(
              text: i10n.aboutText3,
              style: DefaultTextStyle.of(context).style
            )
          )
        ),
        Container(
          child: Image.asset('images/cpp_logo.png'),
          decoration: imageDecoration,
        ),
        Container(
          margin: const EdgeInsets.all(16.0),
          child: RichText(
            textAlign: TextAlign.justify,
            text: TextSpan(
              text: i10n.aboutText4,
              style: DefaultTextStyle.of(context).style
            )
          )
        ),
        Container(
          child: Image.asset('images/bluetooth_logo.png'),
          decoration: imageDecoration,
        ),
        Container(
          margin: const EdgeInsets.all(16.0),
          child: RichText(
            textAlign: TextAlign.justify,
            text: TextSpan(
              text: i10n.aboutText5,
              style: DefaultTextStyle.of(context).style
            )
          )
        ),
      ]
    );
  }
}
