import 'package:flutter/material.dart';

import '../../util/ApplicationLocalizations.dart';

class StatusTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(ApplicationLocalizations.of(context).mainPage.menuStatus)
    );
  }
}
