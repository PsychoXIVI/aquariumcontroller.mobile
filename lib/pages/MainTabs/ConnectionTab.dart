import 'package:flutter/material.dart';

import '../../util/ApplicationLocalizations.dart';
import '../../models/ConnectionSettings.dart';

class ConnectionTab extends StatefulWidget {
  //final ConnectionSettings startingSettings;
  
  //

  @override
  _ConnectionTabState createState() {
    return _ConnectionTabState(ConnectionSettings());
  }
}

class _ConnectionTabState extends State<ConnectionTab> {
  ConnectionSettings _settings;

  _ConnectionTabState(this._settings);
  
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        ListTile(title: Text('Ustawienia')),
        ListTile(
          leading: const Icon(Icons.settings_bluetooth),
          title: Text('Obecne podłączone urządzenie'),
          subtitle: Text('11:22:33:44:55:66 - Jacek Akwarium'),
          trailing: const Icon(Icons.more_vert),
        ),
        ListTile(
          title: RaisedButton(
            child: Text('Wybierz urządzenie Bluetooth'),
            onPressed: () async {
              print(':F');
            },
          ),
        ),
        SwitchListTile(
          secondary: _settings.rememberDevice ? const Icon(Icons.link) : const Icon(Icons.link_off),
          title: const Text('Zapamietaj urządzenie'),
          value: _settings.rememberDevice,
          onChanged: (bool value) { setState(() { _settings.rememberDevice = value; }); },
        ),
        SwitchListTile(
          secondary: const Icon(Icons.repeat),
          title: const Text('Połącz automatycznie'),
          value: _settings.keepReconnecting,
          onChanged: (bool value) { setState(() { _settings.keepReconnecting = value; }); },
        ),
        Divider(),
        ListTile(title: Text('Narzędzia')),
        ListTile(
          title: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              FlatButton(
                child: Text('Wyślij testowe zapytanie Ping-Pong'),
                onPressed: () async {
                  return showDialog<void>(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('@TODO . Not implemented!'),
                        actions: <Widget>[
                          FlatButton(
                            child: Text('¯\\_(ツ)_/¯'),
                            onPressed: () { Navigator.of(context).pop(); },
                          ),
                        ],
                      );
                    },
                  );
                },
              ),
              FlatButton(
                child: Text('Wyślij własne zapytanie (z kodu HEX)'),
                onPressed: null,
              ),
              FlatButton(
                child: Text('Wyślij surową wartość (z kodu HEX)'),
                onPressed: null,
              ),
            ]
          )
        ),
        Divider(),
        ListTile(title: Text('Statystyki')),
        ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 24.0),
          title: Table(
            columnWidths: {0: FractionColumnWidth(.75)},
            children: <TableRow>[
              TableRow(children: <Widget>[
                Text('Średni czas odpowiedzi', textAlign: TextAlign.right,),
                Text('  ' + 44.toString()),
              ]),
              TableRow(children: <Widget>[
                Text('Liczba wysłanych pakietów', textAlign: TextAlign.right),
                Text('  ' + 321231.toString()),
              ]),
              TableRow(children: <Widget>[
                Text('Liczba ponownie wysłane pakietów', textAlign: TextAlign.right),
                Text('  ' + 13.toString()),
              ]),
            ]
          )
        ),
      ],
    );
  }
}
