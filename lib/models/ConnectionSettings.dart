
class ConnectionSettings {
  //BluetoothDeviceInfo device;
  bool rememberDevice;
  bool keepReconnecting;

  ConnectionSettings({bool rememberDevice = true, bool keepReconnecting = true}) {
    this.rememberDevice = rememberDevice;
    this.keepReconnecting = keepReconnecting;
  }
}
